<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns:dw="http://www.mulesoft.org/schema/mule/ee/dw" xmlns:apikit="http://www.mulesoft.org/schema/mule/apikit" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:spring="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/apikit http://www.mulesoft.org/schema/mule/apikit/current/mule-apikit.xsd
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/ee/dw http://www.mulesoft.org/schema/mule/ee/dw/current/dw.xsd">

	<configuration doc:name="configuration" defaultExceptionStrategy-ref="global-exception-strategy">
		<expression-language autoResolveVariables="false">
			<import class="org.apache.commons.lang3.StringEscapeUtils" />
			<import class="org.mule.util.ExceptionUtils" />
		</expression-language>
	</configuration>

	<choice-exception-strategy name="global-exception-strategy">

		<catch-exception-strategy when="#[exception.causedBy(us.santander.api.exception.SantanderApiException)]" doc:name="catch-santander-api-exception">
			<set-variable variableName="originalException" value="#[ExceptionUtils.getRootCause(exception)]" doc:name="Save Original Exception" />
			<set-property propertyName="#['http.status']" value="#[flowVars.originalException.getHttpStatus()]" doc:name="Set HTTP Status" />
			<set-property propertyName="#['Content-Type']" value="#['application/json']" doc:name="Set Content Type" />
			<set-variable variableName="errorID" value="#[&quot;${application.id}-${application.layer}-&quot;.concat(flowVars.originalException.getID())]" doc:name="Save Error ID" />
			<set-variable variableName="errorMessage" value="#[flowVars.originalException.getMessage()]" doc:name="Save Error Message" />
            <set-variable variableName="origin" value="${cause.origin}" doc:name="Set origin"/>
			<dw:transform-message doc:name="Set Response">
				<dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	"status": outboundProperties.'http.status' as :number,
	"code": flowVars.errorID as :string,
	"message": flowVars.errorMessage default "No error message recorded",
	"cause": [
    {
		"origin" : flowVars.origin,
		"message" : flowVars.errorMessage default "No error message recorded"
	}
  ]
} ]]></dw:set-payload>
			</dw:transform-message>
			<flow-ref name="resolve-error-message-from-error-catalog" doc:name="resolve-error-message-from-error-catalog" />

		</catch-exception-strategy>

		<catch-exception-strategy when="#[exception.causedBy(org.mule.module.http.internal.request.ResponseValidatorException)]" doc:name="catch-invalid-http-response">
			<set-property propertyName="#['http.status']" value="#[message.inboundProperties['http.status']]" doc:name="Propagate HTTP Status" />
			<set-property propertyName="#['Content-Type']" value="#[message.inboundProperties['Content-Type']]" doc:name="Propagate Content Type" />
			<set-payload value="#[message.payloadAs(java.lang.String)]" doc:name="Propagate HTTP Response" />
		</catch-exception-strategy>
		<catch-exception-strategy doc:name="catch-generic-exception">
			<set-variable variableName="logLevel" value="ERROR" doc:name="Set error logging level to ERROR" />
			<flow-ref name="analyse_error" doc:name="analyse_error" />
			<flow-ref name="make_error_response" doc:name="make_error_response" />
			<flow-ref name="resolve-error-message-from-error-catalog" doc:name="resolve-error-message-from-error-catalog" />
		</catch-exception-strategy>
	</choice-exception-strategy>

	<sub-flow name="analyse_error">
		<!-- Errors raised by a custom policy do not include an exception object, so we must handle the exception object being null -->
		<set-variable variableName="originalHttpStatus" value="" doc:name="There is no HTTP status from the request" />
		<set-variable variableName="exceptionCause" value="#[ExceptionUtils.getRootCauseMessage(exception)]" doc:name="Record the root cause exception message" />
		<set-variable variableName="exceptionMessage" value="#[ExceptionUtils.getRootCause(exception) != null ? (ExceptionUtils.getRootCause(exception).getMessage() != null ? ExceptionUtils.getRootCause(exception).getMessage() : ExceptionUtils.getRootCause(exception).getClass().getName() + ' (No message available)') : 'No root cause exception']" doc:name="Record the message of the root exception" />
		<set-variable variableName="exceptionMessage" value="#[StringEscapeUtils.escapeJava(flowVars.exceptionMessage)]" doc:name="Escape exception message quotes" />

		<choice doc:name="Do we have an exception object">
			<when expression="#[exception == null]">
				<set-property propertyName="http.status" value="500" doc:name="Set HTTP Status" />
				<set-variable variableName="type" value="${application.id}-${application.layer}-T-9500" doc:name="Variable" />
			</when>
			<otherwise>
				<choice doc:name="Set response fields based on the exception">
					<when expression="#[exception.causedBy(java.net.UnknownHostException) 
                      || exception.causedBy(java.net.SocketTimeoutException) 
                      || exception.causedBy(sun.security.provider.certpath.SunCertPathBuilderException)
                      || exception.causedBy(java.util.concurrent.TimeoutException)
                      || exception.causedBy(java.nio.channels.UnresolvedAddressException)
                      || exception.causedBy(java.net.ConnectException)]">
						<set-property propertyName="http.status" value="504" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-T-9504" doc:name="Variable" />
					</when>
					<when expression="#[exception.causedBy(org.mule.module.apikit.exception.BadRequestException)]">
						<set-property propertyName="http.status" value="400" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-F-9400" doc:name="Set Type" />
					</when>
					<when expression="#[exception.causedBy(org.mule.module.apikit.exception.NotFoundException)]">
						<set-property propertyName="http.status" value="404" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-F-9404" doc:name="Set Type" />
					</when>
					<when expression="#[exception.causedBy(org.mule.module.apikit.exception.MethodNotAllowedException)]">
						<set-property propertyName="http.status" value="405" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-F-9405" doc:name="Set Type" />
					</when>
					<when expression="#[exception.causedBy(org.mule.module.apikit.exception.NotAcceptableException)]">
						<set-property propertyName="http.status" value="406" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-F-9406" doc:name="Set Type" />
					</when>
					<when expression="#[exception.causedBy(org.mule.module.apikit.exception.UnsupportedMediaTypeException)]">
						<set-property propertyName="http.status" value="415" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-F-9415" doc:name="Set Type" />
					</when>
					<otherwise>
						<set-property propertyName="http.status" value="500" doc:name="Set HTTP Status" />
						<set-variable variableName="type" value="${application.id}-${application.layer}-T-9500" doc:name="Set Type" />
					</otherwise>
				</choice>
			</otherwise>
		</choice>

	</sub-flow>

	<sub-flow name="make_error_response">
		<set-property propertyName="Content-Type" value="application/json" doc:name="Set Content Type" />
		<set-payload value="{}" doc:name="Zero out any problematic payload" mimeType="application/json" />
		<set-variable variableName="origin" value="${cause.origin}" doc:name="Set origin"/>
		<dw:transform-message doc:name="Create error response payload">
			<dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	"status": outboundProperties.'http.status' as :number when outboundProperties.'http.status' != null otherwise "",
	"code": flowVars.type as :string when flowVars.type != null otherwise "UNKNOWN",
	"message": flowVars.exceptionMessage as :string when flowVars.exceptionMessage != null otherwise "No error message recorded",
	"cause": [
    {
		"origin" : flowVars.origin,
		"message" : flowVars.exceptionCause when flowVars.exceptionCause != null otherwise {
		}
	}
  ]
} ]]></dw:set-payload>
		</dw:transform-message>
		<logger message="type=&quot;#[flowVars.type]&quot; error=&quot;#[flowVars.exceptionMessage]&quot;" level="ERROR" doc:name="Log Formatted Error" />

	</sub-flow>

</mule>
