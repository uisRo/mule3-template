package com.santander.bridge.logging;

import com.santander.logging.Config;

public class BeanBridge {

	public void recordLoggingInfoDetails(String correlationId, String messageId, String appId, String serverId, String processingId, String flowName) {
		Config.setThreadContextLoggingInfoDetails(correlationId,messageId,appId,serverId,processingId,flowName);
	}

	public void recordLoggingInfoFlowName(String flowName) {
		Config.setThreadContextLoggingInfoFlowName(flowName);
	}

	public void recordLoggingInfoProcessingId(String processingId) {
		Config.setThreadContextLoggingInfoProcessingId(processingId);
	}
}
