package com.santander.healthcheck;

/**
 * @author x316881
 *
 */

public class HealthCheckBean {

	/**
	 * Client Name
	 */
	private String client;

	/**
	 * System Status
	 */
	private String status;

	/**
	 * Type System
	 */
	private String type;

	public HealthCheckBean() {
	}

	public HealthCheckBean(String client, String type) {
		this.client = client;
		this.type = type;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "HealthCheckBean [client=" + client + ", status=" + status + ", type=" + type + "]";
	}

	
}
