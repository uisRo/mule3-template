package com.santander.healthcheck;

public enum HealthCheckStatus {

	UP("UP"), DOWN("DOWN"), UNKNOWN("UNKNOWN");

	private final String status;

	private HealthCheckStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}