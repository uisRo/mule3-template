package com.mule.support;

import org.mule.api.MuleEvent;
import org.mule.api.transformer.DataType;
import org.mule.api.transport.PropertyScope;

import com.mulesoft.mule.cache.responsegenerator.ResponseGenerator;

public class PayloadResponseGenerator implements ResponseGenerator {
	private static final String HTTP_STATUS_PROP = "http.status";
	private static final String APPLICATION_JSON = "application/json";
	
	@Override
	public MuleEvent create(MuleEvent request, MuleEvent cachedResponse) {
		final DataType<?> payloadType = cachedResponse.getMessage().getDataType();
		payloadType.setMimeType(APPLICATION_JSON);
		request.getMessage().setPayload(cachedResponse.getMessage().getPayload(), payloadType);
		request.getMessage().setProperty(HTTP_STATUS_PROP, cachedResponse.getMessage().getInboundProperty(HTTP_STATUS_PROP), PropertyScope.INBOUND);
		return request;
	}
}