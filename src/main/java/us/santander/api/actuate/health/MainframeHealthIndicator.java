package us.santander.api.actuate.health;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements health indication for Mainframe systems.
 * @author x198429
 *
 */
public class MainframeHealthIndicator implements HealthIndicator {
	
	Logger log = LoggerFactory.getLogger(MainframeHealthIndicator.class);
	
	private String host;
	private String client;
	private int port;

	/**
	 * Create a new MainframeHealthIndicator with the specified settings.
	 * @param host mainframe's endpoint.
	 * @param port mainframe'port.
	 */
	public MainframeHealthIndicator(String host, Integer port, String client) {
		this.host = host;
		this.port = port;
		this.client = client;
	}
	
	
	public Health health() {
		SocketAddress address = null;
		Socket socket = null;
		int timeout = 10000;
		try {
			address = new InetSocketAddress(this.host, this.port);
			socket = new Socket();
			socket.connect(address, timeout);
		} catch(IOException ioe) {
			Health.Builder builder = new Health.Builder(client);
			return builder.down().build();
		} finally {
			this.close(socket);
		}
		Health.Builder builder = new Health.Builder(client);		
		return builder.up().build();
	}
	
	private void close(Socket socket) {
		try {
			if (socket != null) {
				socket.close();
			}
		} catch (IOException ioe) {
		}
	}

}
