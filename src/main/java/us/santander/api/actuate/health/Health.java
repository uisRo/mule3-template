package us.santander.api.actuate.health;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Carries information about the health of a component or subsystem.
 * @author x198429
 *
 */
@JsonInclude(Include.NON_EMPTY)
public class Health {
	
	private Status status;
	private String client;
	private final Map<String, Object> details;
	
	private Health(Builder builder) {
		this.status = builder.status;
		this.client = builder.client;
		this.details = Collections.unmodifiableMap(builder.details);
	}
	
	/**
	 * Returns the status of the health.
	 * @return the status
	 */
	@JsonUnwrapped
	public Status getStatus() {
		return this.status;
	}
	
	public void setStatus(Status status){
		this.status = status;
	}
	/**
	 * Returns the details for this health
	 * @return the details (or an empty map)
	 */
	public Map<String, Object> getDetails() {
		return this.details;
	}
	
	public String getClient() {
		return client;
	}

	/**
	 * Builder for creating static instances of {@link Health}
	 */
	public static class Builder {
		private Status status;
		private String client;
		private Map<String, Object> details;
		
		/**
		 * Creates a new {@link Builder} instance.
		 */
		public Builder() {
			this.status = Status.UNKNOWN;			
			this.details = new LinkedHashMap<String, Object>();
		}
		
		/**
		 * Creates a new {@link Builder} instance.
		 */
		public Builder(String client) {
			this.status = Status.UNKNOWN;
			this.client = client;
			this.details = new LinkedHashMap<String, Object>();
		}
		
		/** 
		 * Creates a new {@link Builder} with the given status.
		 * @param status the {@link Status} to use
		 */
		public Builder(Status status) {
			this.status = status;
			this.details = new LinkedHashMap<String, Object>();
		}
		
		/** 
		 * Creates a new {@link Builder} with the given status and details.
		 * @param status the {@link Status} to use
		 * @param details the details {@link Map} to use 
		 */
		public Builder(Status status, LinkedHashMap<String, Object> details) {
			this.status = status;
			this.details = details;
		}
		
		/**
		 * Sets status to {@link Status#UP}
		 * @return this {@link Builder} instance
		 */
		public Builder up() {
			return status(Status.UP);
		}
		
		/**
		 * Sets status to {@link Status#DOWN}
		 * @return this {@link Builder} instance
		 */
		public Builder down() {
			return status(Status.DOWN);
		}
		
		/**
		 * Sets status to given {@link Status}
		 * @param status the status
		 * @return this {@link Builder} instance
		 */
		public Builder status(Status status) {
			this.status = status;
			return this;
		}
		
		/**
		 * Creates an instance of {@link Health} with the previously specified status and details
		 * @return a new {@link Health} instance
		 */
		public Health build() {
			return new Health(this);
		}
	}
}
