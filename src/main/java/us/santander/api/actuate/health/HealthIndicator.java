package us.santander.api.actuate.health;

/**
 * Interface to provide an indication of application health.
 * @author x198429
 *
 */
public interface HealthIndicator {
	/**
	 * Return an indication of health.
	 * @return the health for the application.
	 */
	Health health();
}
