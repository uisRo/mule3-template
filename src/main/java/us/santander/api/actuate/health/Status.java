package us.santander.api.actuate.health;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Value object to express state of a component or subsystem
 * @author x198429
 *
 */
public class Status {
	private final String code;
	
	private final String description;
	
	/** {@link Status} indicating that the component is in an unknown state */
	public static final Status UNKNOWN = new Status("UNKNOWN");
	
	/** {@link Status} indicating that the component is functioning as expected */
	public static final Status UP = new Status("UP");
	
	/** {@link Status} indicating that the component is experiencing an unexpected failure */
	public static final Status DOWN = new Status("DOWN");
	
	/**
	 * Creates a new {@link Status} instance with the given code and an empty description.
	 * @param code
	 * @param description
	 */
	public Status(String code) {
		this(code, "");
	}
	
	/**
	 * Creates a new {@link Status} instance with the given code and description.
	 * @param code the status code
	 * @param description a description of the status
	 */
	public Status(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Returns the code for this status.
	 * @return the code
	 */
	@JsonProperty("status")
	public String getCode() {
		return this.code;
	}
	
	/**
	 * Returns a description for this status.
	 * @return the description
	 */
	@JsonInclude(Include.NON_EMPTY)
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj instanceof Status) {
			if (this.code != null) {
				return this.code.equals(((Status) obj).code);
			}
			return false;
		}
		return false;
	}
}
