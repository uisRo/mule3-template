package us.santander.api;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ApplicationsConfigHelper {

	private final static String emitter = "emitter";
	private final static String keyid = "keyid";
	private final static String usertype = "usertype";
	private final static String genericuser = "genericuser";
	private final static String tokentype = "tokentype";
	private final static String RACF_KEY_ID = "racfkid";
	private final static String channel = "channel";
	
	private String appConfig;
	private JSONObject configJSON;
	
	public String getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(String appConfig) {
		this.appConfig = appConfig;
		JSONParser parser = new JSONParser();
		try {
			this.configJSON =(JSONObject) parser.parse(this.appConfig);
		} catch (ParseException e) {
			throw new IllegalArgumentException ("Not a valid JSON Object");
		}
	}
	
	public String getEmitter (String confID){
		
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}
		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(emitter).toString();
	}
	
	public String getKeyID (String confID){
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}

		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(keyid).toString();
	}
	
	public String getUserType (String confID){
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}

		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(usertype).toString();
	}

	public String getGenericUser (String confID){
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}

		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(genericuser).toString();
	}

	public String getTokenType (String confID){
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}

		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(tokentype).toString();
	}
	
	public String getRacfKeyId (String confId) {
		return this.getProperty(confId, RACF_KEY_ID);
	}

	public String getChannel (String confID){
		if (this.configJSON.get(confID) == null){
			throw new IllegalArgumentException ("No config found for that id");
		}

		JSONObject config =  ((JSONObject)this.configJSON.get(confID));
		return config.get(channel).toString();
	}
	
	private String getProperty(String confId, String propertyId) {
		JSONObject config =  ((JSONObject)this.configJSON.get(confId));
		if (config == null) {
			throw new IllegalArgumentException ("No config found for that id");
		}
		return config.get(propertyId).toString();
	}
	
}