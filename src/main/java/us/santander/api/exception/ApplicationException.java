package us.santander.api.exception;

import java.util.Arrays;
import java.util.List;

/**
 * Base class for all application specific errors. Adds an error code property
 * that corresponds with the global error catalog.
 * 
 * It also allows to provide a List of additional data to further describe the
 * cause of the exception.
 * 
 * @author x198429
 *
 */
public class ApplicationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2138276724354293302L;

	private String code;
	private List<Object> data;

	/**
	 * Creates a new Application exception with a default message and an error code.
	 * 
	 * @param code
	 *            Code according to global error catalog.
	 * @param message
	 *            Default message to use for this exception.
	 * @param cause
	 *            The cause of this exception.
	 */
	public ApplicationException(String code, String message, Throwable cause) {
		super(message, cause);
		this.setCode(code);
	}

	/**
	 * Creates a new Application exception with default message and error code.
	 * 
	 * @param code
	 *            Code according to global error catalog.
	 * @param defaultMessage
	 *            Default error message.
	 * 
	 */
	public ApplicationException(String code, String defaultMessage) {
		super(defaultMessage);
		this.setCode(code);
	}

	/**
	 * Creates a new ApplicationException with an error code and default message, it
	 * allows to provide additional data to describe the cause.
	 * 
	 * @param code
	 *            Error code according to global error catalog.
	 * @param defaultMessage
	 *            Default error message.
	 * @param data
	 *            Additional error details.
	 */
	public ApplicationException(String code, String defaultMessage, Object[] data) {
		this(code, defaultMessage);
		if (data != null) {
			this.setData(Arrays.asList(data));
		}
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}
}
