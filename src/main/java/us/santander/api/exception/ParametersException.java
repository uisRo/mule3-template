package us.santander.api.exception;

public final class ParametersException extends BadRequestException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ParametersException(String message) {
		super(message);
	}
	
	@Override
	public String getCode() {
		return "0000";
	}
}
