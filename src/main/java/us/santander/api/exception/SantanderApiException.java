package us.santander.api.exception;

public abstract class SantanderApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private static final String applicationID = "ATM_DOCUMENTARY_PAYMENTS";
	private static final String applicationType = "S";

	public final String getApplicationID() {
		return applicationID;
	}

	public final String getApplicationType() {
		return applicationType;
	}

	public abstract String getCode();

	public abstract String getType();

	public final String getID() {

		return String.format("%s-%s-%s-%s", 
				this.getApplicationID(), 
				this.getApplicationType(), 
				this.getType(),
				this.getCode());
	}
	
	public abstract Integer getHttpStatus();
	
	public SantanderApiException(String message) {
		super(message);
	}
}
