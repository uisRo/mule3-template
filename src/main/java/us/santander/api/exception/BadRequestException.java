package us.santander.api.exception;

public abstract class BadRequestException extends SantanderApiException {

	private static final long serialVersionUID = 1L;

	public BadRequestException(String message) {
		super(message);
	}
	
	@Override
	public String getType() {
		return "F";
	}
	
	@Override
	public Integer getHttpStatus() {
		return 400;
	}
}
