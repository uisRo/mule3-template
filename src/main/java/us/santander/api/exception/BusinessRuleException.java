package us.santander.api.exception;

public abstract class BusinessRuleException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	
	public BusinessRuleException(String message) {
		super(message);
	}

	@Override
	public Integer getHttpStatus() {
		return 412;
	}

}
