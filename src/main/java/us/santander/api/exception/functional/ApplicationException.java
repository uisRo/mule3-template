/**
 * 
 */
package us.santander.api.exception.functional;

import us.santander.api.exception.UnexpectedException;

/**
 * @author x329503
 *
 */
public class ApplicationException extends UnexpectedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ApplicationException(String message) {
		super(message);
	}

	@Override
	public String getCode() {
		return "9500";
	}

}
