package us.santander.api.exception.functional;

import us.santander.api.exception.BadRequestException;

public final class ParametersException extends BadRequestException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ParametersException(String message) {
		super(message);
	}
	
	@Override
	public String getCode() {
		return "9400";
	}
}
