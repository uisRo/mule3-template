package us.santander.api.exception.functional;

import us.santander.api.exception.BusinessRuleException;

public class StructureDataException extends BusinessRuleException {

	private static final long serialVersionUID = 1L;

	public StructureDataException(String message) {
		super(message);
	}

	@Override
	public String getCode() {
		return "0400";
	}
	
}
