package us.santander.api.exception.functional;

import us.santander.api.exception.ResourceNotFoundException;

public class NotFoundException extends ResourceNotFoundException {

	private static final long serialVersionUID = 1L;

	public NotFoundException(String message) {
		super(message);
	}

	@Override
	public String getCode() {
		return "0404";
	}
	
}
