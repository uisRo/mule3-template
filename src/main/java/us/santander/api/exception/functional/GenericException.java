package us.santander.api.exception.functional;

import us.santander.api.exception.SantanderApiException;

public class GenericException extends SantanderApiException {

	private static final long serialVersionUID = 1L;
	private String requestCode;
	private Integer requestHttpStatus;
	private String requestType;

	public GenericException(String message) {
		super(message);
	}

	public GenericException(final String message, final Integer httpStatus, final String code,final String type) {
		super(message);
		requestHttpStatus = httpStatus;
		requestCode = code;
		requestType = type;
	}

	@Override
	public String getCode() {
		return getRequestCode();
	}

	@Override
	public String getType() {
		return getRequestType();
	}

	@Override
	public Integer getHttpStatus() {
		return getRequestHttpStatus();
	}

	public String getRequestCode() {
		return requestCode;
	}

	public Integer getRequestHttpStatus() {
		return requestHttpStatus;
	}

	public String getRequestType() {
		return requestType;
	}

}