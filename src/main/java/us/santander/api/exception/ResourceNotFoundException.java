package us.santander.api.exception;

public abstract class ResourceNotFoundException extends BadRequestException {

	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException(String message) {
		super(message);
	}
	
	@Override
	public final Integer getHttpStatus() {
		return 404;
	}
}
