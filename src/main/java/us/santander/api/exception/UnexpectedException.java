package us.santander.api.exception;

public abstract class UnexpectedException extends SantanderApiException {

	private static final long serialVersionUID = 1L;

	public UnexpectedException(String message) {
		super(message);
	}
	
	@Override
	public String getType() {
		return "T";
	}
	
	@Override
	public Integer getHttpStatus() {
		return 500;
	}
}
